<?php

namespace Drupal\feeds_paged_drupal_jsonapi_fetcher\Feeds\Fetcher;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\feeds\Exception\EmptyFeedException;
use Drupal\feeds\FeedInterface;
use Drupal\feeds\Plugin\Type\ClearableInterface;
use Drupal\feeds\Plugin\Type\Fetcher\FetcherInterface;
use Drupal\feeds\Plugin\Type\PluginBase;
use Drupal\feeds\Result\FetcherResult;
use Drupal\feeds\Result\HttpFetcherResult;
use Drupal\feeds\StateInterface;
use Drupal\feeds\Utility\Feed;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\feeds\Feeds\Fetcher\HttpFetcher;

/**
 * Defines an HTTP fetcher.
 *
 * @FeedsFetcher(
 *   id = "pageddrupaljsonapifetcher",
 *   title = @Translation("Paged Drupal Jsonapi Fetcher"),
 *   description = @Translation("Downloads paged results from a Drupal jsonapi endpoint."),
 *   form = {
 *     "configuration" = "Drupal\feeds\Feeds\Fetcher\Form\HttpFetcherForm",
 *     "feed" = "Drupal\feeds\Feeds\Fetcher\Form\HttpFetcherFeedForm",
 *   }
 * )
 */
class PagedDrupalJsonapiFetcher extends HttpFetcher{

  /**
   * {@inheritdoc}
   */
  public function fetch(FeedInterface $feed, StateInterface $state) {

    $sink = $this->fileSystem->tempnam('temporary://', 'feeds_http_fetcher');
    $sink = $this->fileSystem->realpath($sink);

    // If we've got a "next" page from the last one
    if(isset($state->nextUrl)) {
      $response = $this->get($state->nextUrl, $sink, '');
    }
    // If this is the first page
    else {
      $response = $this->get($feed->getSource(), $sink, '');
    }

    // Get json decoded response body
    $responseJson = json_decode($response->getBody());

    // If there are more pages
    if(isset($responseJson->links->next->href)) {

        // Save the next url in the state so we can get it next run
        $state->nextUrl = $responseJson->links->next->href;
        if( !isset($state->total) || !$state->total ) {
          $state->total = 1;
        }
        // Up the total fetches by one
        $state->total = $state->total + 1;

        // We don't know how many there are so say we're one away from one more
        $state->progress($state->total, $state->total - 1);
    }
    // If this is the last page
    else {
      $state->progress($state->total,$state->total);
      $state->setCompleted();
    }

    return new HttpFetcherResult($sink, $response->getHeaders());
  }

  /**
   * Performs a GET request.
   *
   * @param string $url
   *   The URL to GET.
   * @param string $sink
   *   The location where the downloaded content will be saved. This can be a
   *   resource, path or a StreamInterface object.
   * @param string $cache_key
   *   (optional) The cache key to find cached headers. Defaults to false.
   *
   * @return \Guzzle\Http\Message\Response
   *   A Guzzle response.
   *
   * @throws \RuntimeException
   *   Thrown if the GET request failed.
   *
   * @see \GuzzleHttp\RequestOptions
   */
  protected function get($url, $sink, $cache_key = FALSE) {
    $url = Feed::translateSchemes($url);

    $options = [RequestOptions::SINK => $sink];

    // Add cached headers if requested.
    if ($cache_key && ($cache = $this->cache->get($cache_key))) {
      if (isset($cache->data['etag'])) {
        $options[RequestOptions::HEADERS]['If-None-Match'] = $cache->data['etag'];
      }
      if (isset($cache->data['last-modified'])) {
        $options[RequestOptions::HEADERS]['If-Modified-Since'] = $cache->data['last-modified'];
      }
    }

    try {
      $response = $this->client->get($url, $options);
    }
    catch (RequestException $e) {
      $args = ['%site' => $url, '%error' => $e->getMessage()];
      throw new \RuntimeException($this->t('The feed from %site seems to be broken because of error "%error".', $args));
    }

    if ($cache_key) {
      $this->cache->set($cache_key, array_change_key_case($response->getHeaders()));
    }

    return $response;
  }

}
